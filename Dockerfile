FROM alpine:3.5

ENV GIT_LFS_VERSION 2.0.1
ENV GIT_LFS_SHA256 e464aa3e13fe47190827960443f1060a814793594a966839c55aa2bbaaf7f752
ENV GIT_LFS_SOURCE https://github.com/github/git-lfs/releases/download/v$GIT_LFS_VERSION/git-lfs-linux-amd64-$GIT_LFS_VERSION.tar.gz

RUN \
	apk add --update --no-cache \
		git \
		openssh-client \

	&& apk add --virtual .build-deps \
		curl \

	&& set -x \

	# git-lfs
    && curl -fSL $GIT_LFS_SOURCE -o git-lfs.tar.gz  \
    && echo "${GIT_LFS_SHA256} *git-lfs.tar.gz" | sha256sum -c - \
    && mkdir git-lfs \
    && tar -xvf git-lfs.tar.gz -C git-lfs --strip-components=1 \
	&& mv git-lfs/git-lfs /usr/bin/ \
	&& rm -Rf git-lfs \

	# Removing build dependencies, clean temporary files
	&& apk del .build-deps \
	&& rm -rf /tmp/* /var/cache/apk/*